package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.Message;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.exception.SQLRuntimeException;

public class MessageDao {

    public void insert(Connection connection, Message message) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO messages ( ");
            sql.append("    user_id, ");
            sql.append("    text, ");
            sql.append("    created_date, ");
            sql.append("    updated_date ");
            sql.append(") VALUES ( ");
            sql.append("    ?, ");                  // user_id
            sql.append("    ?, ");                  // text
            sql.append("    CURRENT_TIMESTAMP, ");  // created_date
            sql.append("    CURRENT_TIMESTAMP ");   // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, message.getUserId());
            ps.setString(2, message.getText());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    //messagesテーブルから特定のmessageIdに紐づくデータを削除
	public Message delete(Connection connection, int deleteId) {

        PreparedStatement ps = null;
        try {
        	String deleteMessageId = String.valueOf(deleteId);
        	String sql = "DELETE FROM messages WHERE id = ? ";

        	ps = connection.prepareStatement(sql);
        	ps.setString(1, deleteMessageId);

            ps.executeUpdate();
            return null;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
	}
	//つぶやき編集の前にデータベースから選択したつぶやきのデータを取り出して編集画面に出す用
	public Message select(Connection connection, int id) {

        PreparedStatement ps = null;
        try {

            String sql = "SELECT * FROM messages WHERE id = ?";

            ps = connection.prepareStatement(sql);

            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();

            List<Message> messages = toMessages(rs);
            if(messages.isEmpty()) {
            	return null ;
            } else {
            	return messages.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
	//上のメソッドで使用しているtoMessagesを成り立たせるためのメソッド
	private List<Message> toMessages(ResultSet rs) throws SQLException {

        List<Message> messages = new ArrayList<Message>();
        try {
            while (rs.next()) {
            	Message message = new Message();
                message.setId(rs.getInt("id"));
                message.setUserId(rs.getInt("user_id"));
                message.setText(rs.getString("text"));

                message.setCreatedDate(rs.getTimestamp("created_date"));
                message.setUpdatedDate(rs.getTimestamp("updated_date"));

                messages.add(message);
            }
            return messages;
        } finally {
            close(rs);
        }
    }

	public void update(Connection connection, Message message) {
        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE messages SET ");
            sql.append("    text = ?, ");
            sql.append("    updated_date = CURRENT_TIMESTAMP ");
            sql.append("WHERE id = ?");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, message.getText());
            ps.setInt(2, message.getId());

            int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
}