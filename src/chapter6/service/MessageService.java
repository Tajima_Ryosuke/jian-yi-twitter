package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

//StringUtilsを使うときインポートする
import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;

public class MessageService {

    public void insert(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().insert(connection, message);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public List<UserMessage> select(String inpStartDate, String inpEndDate, String userId) {
        final int LIMIT_NUM = 1000;

        Connection connection = null;
        try {
            connection = getConnection();

            Integer id = null;
            if(!StringUtils.isEmpty(userId)) {
              id = Integer.parseInt(userId);
            }

          //開始日
            Calendar startCalender = Calendar.getInstance();
            startCalender.set(2020, 0, 1, 0, 0, 0);
        	Date start = startCalender.getTime();
        	SimpleDateFormat startDay = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        	String strStartDay = startDay.format(start);

        	if(!StringUtils.isEmpty(inpStartDate)) {
        		strStartDay = inpStartDate + " 00:00:00" ;
        	}

        	//終了日
        	Date end = new Date() ;
        	SimpleDateFormat endDay = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        	String strEndDay = endDay.format(end);

        	if(!StringUtils.isEmpty(inpEndDate)) {
        		strEndDay = inpEndDate + " 23:59:59" ;
        	}

            List<UserMessage> messages = new UserMessageDao().select(connection, id, LIMIT_NUM, strStartDay, strEndDay);

            commit(connection);

            return messages;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    //メッセージの削除
    public void delete(int deleteId) {

        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().delete(connection, deleteId);
            commit(connection);
            return ;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
  //メッセージの参照
  	public Message select(int id) {

        Connection connection = null;
        try {
            connection = getConnection();
            Message message = new MessageDao().select(connection, id);
            commit(connection);
            return message;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //メッセージの更新
	public Message update(Message message) {

        Connection connection = null;
        String messageText = message.getText();
        try {
        	//textが空のときはメッセージ編集したくないから条件分岐のIF
        	if(StringUtils.isEmpty(messageText)) {
            return null ;
        	}
            connection = getConnection();
            new MessageDao().update(connection, message);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
		return message;
    }
}