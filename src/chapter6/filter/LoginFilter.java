package chapter6.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter6.beans.User;

@WebFilter(urlPatterns= {"/setting","/edit"})
public class LoginFilter implements Filter{
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest rq = (HttpServletRequest)request;
		HttpServletResponse rp = (HttpServletResponse)response;
		HttpSession session = rq.getSession();

//		@SuppressWarnings("unchecked")
		User LoginUsers = (User)session.getAttribute("loginUser");

		if(LoginUsers == null) {
			List<String> errorMessages = new ArrayList<String>();
            errorMessages.add("ログインしてください");
            session.setAttribute("errorMessages", errorMessages);

            rp.sendRedirect("./login");
            return;
		}
		//Servletへ処理を移動させるためのコード
		chain.doFilter(request, response);
	}

	//今回は記述しないけれどもFilterを動作させるために必要
	//initは開始 destroyは停止で使われる
	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO 自動生成されたメソッド・スタブ
	}

	@Override
	public void destroy() {
		// TODO 自動生成されたメソッド・スタブ
	}
}
