package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String selectMessageId = request.getParameter("updataMessage");
		List<String> errorMessages = new ArrayList<String>();

		Message message = null ;

		if((!StringUtils.isBlank(selectMessageId)) && (selectMessageId.matches("^[0-9].+$"))) {
			int updateMessageId = Integer.parseInt(selectMessageId);
			message = new MessageService().select(updateMessageId);
		}

		//DBにないidが入力されたときにDaoから返されるnullを生かしてエラー登録してる
		if(message != null) {
			request.setAttribute("updataMessage", message);
			request.getRequestDispatcher("edit.jsp").forward(request, response);
		}else {
			errorMessages.add("不正なパラメータが入力されました");
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("./").forward(request, response);
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<String> errorMessages = new ArrayList<String>();

		String text = request.getParameter("text");

		String selectMessage = request.getParameter("id");
		int updateMessageId = Integer.parseInt(selectMessage);

		Message message =getMessage(request);
		message.setText(text);
		message.setId(updateMessageId);

		if (isValid(message, errorMessages)) {
			try {
				new MessageService().update(message);
			} catch(NoRowsUpdatedRuntimeException e) {
				errorMessages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
			}
		}

		if (errorMessages.size() != 0) {
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("message", message);
			request.getRequestDispatcher("/edit.jsp").forward(request, response);
			return ;
		}

		response.sendRedirect("./");
	}

	private Message getMessage(HttpServletRequest request) throws IOException, ServletException {

		Message message = new Message();
		message.setId(Integer.parseInt(request.getParameter("id")));
		message.setText(request.getParameter("text"));

		return message;
	}

	private boolean isValid(Message message, List<String> errorMessages) {
		String text = message.getText();

		if (StringUtils.isBlank(text)) {
			errorMessages.add("メッセージを入力してください");
			return false;
		} else if (140 < text.length()) {
			errorMessages.add("140文字以下で入力してください");
			return false;
		}

		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}

