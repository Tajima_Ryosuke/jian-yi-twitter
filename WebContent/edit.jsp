<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>簡易Twitter</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>Insert title here</title>
</head>
<body>
	<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<div class="form-area">
			<form action="edit" method="post">
				<label for="text">つぶやき</label>
				<input name ="id" value="${updataMessage.id}" id="id" type="hidden">
				<textarea name="text" cols="100" rows="5" class="tweet-box"><c:out value="${updataMessage.text}" /></textarea>
				<br /> <input type="submit" value="更新">（140文字まで）
				<a href="./">戻る</a>
			</form>
		</div>
		<div class="copyright">Copyright(c)YourName</div>
	</div>
</body>
</html>